using UniversitySystem.Interfaces;
using UniversitySystem.Contexts;
using UniversitySystem.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace UniversitySystem.Extensions
{
    public static class DependencyInjection
    {
        public static ServiceProvider Provider()
        {
            var serviceCollection = new ServiceCollection();

            serviceCollection.AddSingleton<IAddressRepository, AddressRepository>();
            serviceCollection.AddSingleton<ICourseDisciplineRepository, CourseDisciplineRepository>();
            serviceCollection.AddSingleton<ICourseRepository, CourseRepository>();
            serviceCollection.AddSingleton<IDepartmentRepository, DepartmentRepository>();
            serviceCollection.AddSingleton<IDependentRepository, DependentRepository>();
            serviceCollection.AddSingleton<IDisciplineRepository, DisciplineRepository>();
            serviceCollection.AddSingleton<IOfferRepository, OfferRepository>();
            serviceCollection.AddSingleton<IProfessorRepository, ProfessorRepository>();
            serviceCollection.AddSingleton<IStudentRepository, StudentRepository>();
            serviceCollection.AddSingleton<ITitleRepository, TitleRepository>();

            serviceCollection.AddDbContext<ContextMaintenance>();
            
            return serviceCollection.BuildServiceProvider();

        }
    }

}