using UniversitySystem.Interfaces;
using UniversitySystem.Entities;
using UniversitySystem.Contexts;

namespace UniversitySystem.Repositories
{
    public class CourseDisciplineRepository : BaseRepository<CourseDiscipline>, ICourseDisciplineRepository
    {
        public CourseDisciplineRepository(ContextMaintenance contextMaintenance) : base(contextMaintenance)
        {

        }
    }
}