using UniversitySystem.Interfaces;
using UniversitySystem.Entities;
using UniversitySystem.Contexts;

namespace UniversitySystem.Repositories
{
    public class OfferRepository : BaseRepository<Offer>, IOfferRepository
    {
        public OfferRepository(ContextMaintenance contextMaintenance) : base(contextMaintenance)
        {

        }
    }
}
