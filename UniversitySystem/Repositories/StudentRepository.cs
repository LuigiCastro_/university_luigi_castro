using UniversitySystem.Interfaces;
using UniversitySystem.Entities;
using UniversitySystem.Contexts;

namespace UniversitySystem.Repositories
{
    public class StudentRepository : BaseRepository<Student>, IStudentRepository
    {
        public StudentRepository(ContextMaintenance contextMaintenance) : base(contextMaintenance)
        {
                
        }

    }
}
