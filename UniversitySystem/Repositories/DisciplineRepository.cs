using UniversitySystem.Interfaces;
using UniversitySystem.Entities;
using UniversitySystem.Contexts;

namespace UniversitySystem.Repositories
{
    public class DisciplineRepository : BaseRepository<Discipline>, IDisciplineRepository
    {
        public DisciplineRepository(ContextMaintenance contextMaintenance) : base(contextMaintenance)
        {

        }
    }
}
