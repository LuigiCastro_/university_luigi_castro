using UniversitySystem.Interfaces;
using UniversitySystem.Entities;
using UniversitySystem.Contexts;

namespace UniversitySystem.Repositories
{
    public class DependentRepository : BaseRepository<Dependent>, IDependentRepository
    {
        public DependentRepository(ContextMaintenance contextMaintenance) : base(contextMaintenance)
        {

        }
    }
}
