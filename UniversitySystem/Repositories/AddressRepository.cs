using UniversitySystem.Interfaces;
using UniversitySystem.Entities;
using UniversitySystem.Contexts;

namespace UniversitySystem.Repositories
{
    public class AddressRepository : BaseRepository<Address>, IAddressRepository
    {
        public AddressRepository(ContextMaintenance contextMaintenance) : base(contextMaintenance)
        {

        }
    }
}
