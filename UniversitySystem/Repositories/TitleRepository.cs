using UniversitySystem.Interfaces;
using UniversitySystem.Entities;
using UniversitySystem.Contexts;

namespace UniversitySystem.Repositories
{
    public class TitleRepository : BaseRepository<Title>, ITitleRepository
    {
        public TitleRepository(ContextMaintenance contextMaintenance) : base(contextMaintenance)
        {

        }
    }
}