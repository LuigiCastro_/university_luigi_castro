using UniversitySystem.Entities;

namespace UniversitySystem.Interfaces
{
    public interface IAddressRepository : IBaseRepository<Address>
    {

    }
}