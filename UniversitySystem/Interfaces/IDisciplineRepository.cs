using UniversitySystem.Entities;

namespace UniversitySystem.Interfaces
{
    public interface IDisciplineRepository : IBaseRepository<Discipline>
    {
        
    }
}