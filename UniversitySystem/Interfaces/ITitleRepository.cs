using UniversitySystem.Entities;

namespace UniversitySystem.Interfaces
{
    public interface ITitleRepository : IBaseRepository<Title>
    {

    }
    
}