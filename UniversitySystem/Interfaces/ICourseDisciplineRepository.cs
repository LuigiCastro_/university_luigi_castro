using UniversitySystem.Entities;

namespace UniversitySystem.Interfaces
{
    public interface ICourseDisciplineRepository : IBaseRepository<CourseDiscipline>
    {

    }
}