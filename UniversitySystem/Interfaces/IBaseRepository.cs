namespace UniversitySystem.Interfaces
{
    public interface IBaseRepository<T> where T : class
    {
        Task AddAsync(T item);
        Task RemoveAsync(T item);
        Task EditAsync(T item);
        Task<T> FindAsync(int id);
        Task<List<T>> ListAsync();
    }
}