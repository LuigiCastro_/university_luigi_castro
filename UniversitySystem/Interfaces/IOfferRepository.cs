using UniversitySystem.Entities;

namespace UniversitySystem.Interfaces
{
    public interface IOfferRepository : IBaseRepository<Offer>
    {

    }
    
}