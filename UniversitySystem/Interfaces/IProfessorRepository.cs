using UniversitySystem.Entities;

namespace UniversitySystem.Interfaces
{
    public interface IProfessorRepository : IBaseRepository<Professor>
    {

    }
}