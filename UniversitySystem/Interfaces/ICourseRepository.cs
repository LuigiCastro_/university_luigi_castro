using UniversitySystem.Entities;

namespace UniversitySystem.Interfaces
{
    public interface ICourseRepository : IBaseRepository<Course>
    {

    }
}