using UniversitySystem.Entities;

namespace UniversitySystem.Interfaces
{
    public interface IDependentRepository : IBaseRepository<Dependent>
    {

    }
}