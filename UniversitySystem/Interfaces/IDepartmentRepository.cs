using UniversitySystem.Entities;

namespace UniversitySystem.Interfaces
{
    public interface IDepartmentRepository : IBaseRepository<Department>
    {

    }
}