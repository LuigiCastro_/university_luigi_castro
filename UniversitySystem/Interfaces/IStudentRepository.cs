using UniversitySystem.Entities;

namespace UniversitySystem.Interfaces
{
    public interface IStudentRepository : IBaseRepository<Student>
    {
        
    }
}