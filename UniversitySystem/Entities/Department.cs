﻿namespace UniversitySystem.Entities;

public class Department : Base
{
    public Department(int addressId, string name)
    {
        AddressId = addressId;
        Name = name;
    }
    public int AddressId { get; set; }
    public Address Address {get; set; }
    public string Name { get; set; }

    public ICollection<Course> Courses {get; set;}
    public ICollection<Professor> Professors {get; set;}
}