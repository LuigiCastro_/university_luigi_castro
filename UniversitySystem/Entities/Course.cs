namespace UniversitySystem.Entities
{
    public class Course : Base
    {
        public Course(int departmentId, string name, string shift, int duration, string typeCourse)
        {
            DepartmentId = departmentId;
            Name = name;
            Shift = shift;
            Duration = duration;
            this.TypeCourse = typeCourse;
        }
        public int DepartmentId { get; set; }
        public Department Department { get; set; }
        public string Name { get; set; }
        public string Shift { get; set; }
        public int Duration { get; set; }                    
        public string TypeCourse { get; set; }

        public ICollection<CourseDiscipline> CourseDisciplines {get; set;}
        public ICollection<Student> Students {get; set;}
    }
}