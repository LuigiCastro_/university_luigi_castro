namespace UniversitySystem.Entities
{
    public class Offer : Base
    {
        public Offer(int studentId, int professorId, int disciplineId, int year, int semester, int note, string frequency)
        {
            StudentId = studentId;
            ProfessorId = professorId;
            DisciplineId = disciplineId;
            Year = year;
            Semester = semester;
            Note = note;
            Frequency = frequency;
        }
        public int StudentId { get; set; }
        public Student Student { get; set; }
        public int ProfessorId { get; set; }
        public Professor Professor { get; set; }
        public int DisciplineId { get; set; }
        public Discipline Discipline { get; set; }
        public int Year { get; set; }
        public int Semester { get; set; }
        public int Note { get; set; }
        public string Frequency { get; set; }
    }

    
}