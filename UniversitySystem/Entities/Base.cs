namespace UniversitySystem.Entities
{
    public class Base
    {
        public Base()
        {
            Active = true;
        }

        public int Id {get; set; }
        public bool Active {get; set; }
        public DateTime CreationDate {get; set; }
        public DateTime? AlterationDate {get; set; }
    }
}