namespace UniversitySystem.Entities
{
    public class CourseDiscipline : Base
    {
        public CourseDiscipline(int courseId, int disciplineId)
        {
            CourseId = courseId;
            DisciplineId = disciplineId;
        }
        public int CourseId { get; set; }
        public Course Course { get; set; }
        public int DisciplineId { get; set; }
        public Discipline Discipline { get; set; }
    }
}
