namespace UniversitySystem.Entities
{
    public class Student : Base
    {
        public Student(int addressId, int courseId, string registry, string name, string birthDate)
        {
            AddressId = addressId;
            CourseId = courseId;
            Registry = registry;
            Name = name;
            BirthDate = birthDate;
        }
        public int AddressId { get; set; }
        public Address Address { get; set; }
        public int CourseId { get; set; }
        public Course Course { get; set; }
        public string Registry { get; set; }
        public string Name { get; set; }
        public string BirthDate { get; set; }

        public ICollection<Offer> Offers {get; set;}

    }
}