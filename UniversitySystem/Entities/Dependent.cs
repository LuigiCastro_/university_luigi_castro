namespace UniversitySystem.Entities
{
    public class Dependent : Base
    {
        public Dependent(int professorId, string name, string relationship, string birthDate)
        {
            ProfessorId = professorId;
            Name = name;
            Relationship = relationship;
            BirthDate = birthDate;

        }
        public int ProfessorId { get; set; }
        public Professor Professor { get; set; }
        public string Name { get; set; }
        public string Relationship { get; set; }
        public string BirthDate { get; set; }
    }
}