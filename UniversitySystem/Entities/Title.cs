namespace UniversitySystem.Entities
{
    public class Title : Base 
    {
        public Title(int professorId, int degree, string course, string institution)
        {
            ProfessorId = professorId;
            Degree = degree;
            Course = course;
            Institution = institution;
        }
        public int ProfessorId { get; set; }
        public Professor Professor { get; set; }
        public int Degree { get; set; }
        public string Course { get; set; }
        public string Institution { get; set; }
    }
}