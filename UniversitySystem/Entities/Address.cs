namespace UniversitySystem.Entities
{
    public class Address : Base
    {
        public Address(string street, string city, string state, string complement)
        {
            Street = street;
            City = city;
            State = state;
            Complement = complement;
        }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Complement { get; set; }

        public ICollection<Department> Departments {get; set;}
        public ICollection<Professor> Professors {get; set;}
        public ICollection<Student> Students {get; set;}

    }
}