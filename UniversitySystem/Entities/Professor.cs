namespace UniversitySystem.Entities
{
    public class Professor : Base
    {
        public Professor(int addressId, int departmentId, string CPF, string name, double salary, string birthDate, string startDate)
        {
            AddressId = addressId;
            DepartmentId = departmentId;
            this.CPF = CPF;
            Name = name;
            Salary = salary;
            BirthDate = birthDate;
            StartDate = startDate;
        }
        public int AddressId { get; set; }
        public Address Address { get; set; }
        public int DepartmentId { get; set; }
        public Department Department { get; set; }
        public string CPF { get; set; }                         // INT OR STRING? REGEX
        public string Name { get; set; }
        public double Salary { get; set; }
        public string BirthDate { get; set; }
        public string StartDate { get; set; }

        public ICollection<Offer> Offers {get; set; }
        public ICollection<Dependent> Dependents {get; set; }
        public ICollection<Title> Titles {get; set; }

    }
}