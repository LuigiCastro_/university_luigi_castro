namespace UniversitySystem.Entities
{
    public class Discipline : Base
    {
        public Discipline(string name, int hours)
        {
            Name = name;
            Hours = hours;
        }
        public string Name { get; set; }
        public int Hours { get; set; }
        public ICollection<CourseDiscipline> CourseDisciplines {get; set;}
        public ICollection<Offer> Offers {get; set;}
    }
}