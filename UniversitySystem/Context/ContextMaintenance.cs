using Microsoft.EntityFrameworkCore;
using UniversitySystem.Entities;

namespace UniversitySystem.Contexts
{
	public class ContextMaintenance : DbContext
	{
		public DbSet<Address> Adresses { get; set; }
		public DbSet<Course> Courses {get; set; }
		public DbSet<CourseDiscipline> CourseDisciplines {get; set; }
		public DbSet<Department> Departments { get; set; }
		public DbSet<Dependent> Dependents {get; set; }
		public DbSet<Discipline> Disciplines {get; set; }
		public DbSet<Offer> Offers {get; set; }
		public DbSet<Professor> Professors {get; set; }
		public DbSet<Student> Students {get; set; }
		public DbSet<Title> Titles {get; set; }


		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlServer(@"Server=44.199.200.211,3309;Database=universidade_luigi_castro;User=luigi_castro;Password=PcRgWM8LZLi3haE3BBWu");
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Course>().HasOne(s => s.Department).WithMany(s => s.Courses).OnDelete(DeleteBehavior.Restrict);
			
			modelBuilder.Entity<CourseDiscipline>().HasOne(s => s.Course).WithMany(s => s.CourseDisciplines).OnDelete(DeleteBehavior.Restrict);
			modelBuilder.Entity<CourseDiscipline>().HasOne(s => s.Discipline).WithMany(s => s.CourseDisciplines).OnDelete(DeleteBehavior.Restrict);

			modelBuilder.Entity<Department>().HasOne(s => s.Address).WithMany(s => s.Departments).OnDelete(DeleteBehavior.Restrict);

			modelBuilder.Entity<Dependent>().HasOne(s => s.Professor).WithMany(s => s.Dependents).OnDelete(DeleteBehavior.Restrict);

			modelBuilder.Entity<Offer>().HasOne(s => s.Student).WithMany(s => s.Offers).OnDelete(DeleteBehavior.Restrict);
			modelBuilder.Entity<Offer>().HasOne(s => s.Professor).WithMany(s => s.Offers).OnDelete(DeleteBehavior.Restrict);
			modelBuilder.Entity<Offer>().HasOne(s => s.Discipline).WithMany(s => s.Offers).OnDelete(DeleteBehavior.Restrict);

			modelBuilder.Entity<Professor>().HasOne(s => s.Department).WithMany(s => s.Professors).OnDelete(DeleteBehavior.Restrict);
			modelBuilder.Entity<Professor>().HasOne(s => s.Address).WithMany(s => s.Professors).OnDelete(DeleteBehavior.Restrict);

			modelBuilder.Entity<Student>().HasOne(s => s.Address).WithMany(s => s.Students).OnDelete(DeleteBehavior.Restrict);
			modelBuilder.Entity<Student>().HasOne(s => s.Course).WithMany(s => s.Students).OnDelete(DeleteBehavior.Restrict);

			modelBuilder.Entity<Title>().HasOne(s => s.Professor).WithMany(s => s.Titles).OnDelete(DeleteBehavior.Restrict);
			
		}

		//ENTIDADE QUE CONTEM FOREIGN KEY
		//<ENTIDADE>.HASONE(FOREIGN KEY).WITHMANY(ENTITY COLLECTION)
	}
}
