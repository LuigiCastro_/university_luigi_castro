namespace UniversitySystemTest;

[TestClass]
public class RegisterTest
{
    public readonly IAddressRepository _addressRepository;
    public readonly ICourseRepository _courseRepository;
    public readonly ICourseDisciplineRepository _courseDisciplineRepository;
    public readonly IDepartmentRepository _departmentRepository;
    public readonly IDependentRepository _dependentRepository;
    public readonly IDisciplineRepository _disciplineRepository;
    public readonly IOfferRepository _offerRepository;
    public readonly IProfessorRepository _professorRepository;
    public readonly IStudentRepository _studentRepository;
    public readonly ITitleRepository _titleRepository;    

    public readonly DateTime _creationDate = DateTime.Now;
    public readonly Random random = new Random();

    public RegisterTest()
    {
        _addressRepository = DependencyInjection.Provider().GetService<IAddressRepository>();
        _courseRepository = DependencyInjection.Provider().GetService<ICourseRepository>();
        _courseDisciplineRepository = DependencyInjection.Provider().GetService<ICourseDisciplineRepository>();
        _departmentRepository = DependencyInjection.Provider().GetService<IDepartmentRepository>();
        _dependentRepository = DependencyInjection.Provider().GetService<IDependentRepository>();
        _disciplineRepository = DependencyInjection.Provider().GetService<IDisciplineRepository>();
        _offerRepository = DependencyInjection.Provider().GetService<IOfferRepository>();
        _professorRepository = DependencyInjection.Provider().GetService<IProfessorRepository>();
        _studentRepository = DependencyInjection.Provider().GetService<IStudentRepository>();
        _titleRepository = DependencyInjection.Provider().GetService<ITitleRepository>();
    }

    [TestMethod]
    [DataRow("street1", "city1", "state1", "complement1")]
    [DataRow("street2", "city2", "state2", "complement2")]
    [DataRow("street3", "city3", "state3", "complement3")]
    [DataRow("street4", "city4", "state4", "complement4")]
    [DataRow("street5", "city5", "state5", "complement5")]
    public async Task AddressRegisterTest(string street, string city, string state, string complement)
    {
        var address = new Address(street, city, state, complement);
        address.CreationDate = _creationDate;
        await _addressRepository.AddAsync(address);
        
        Assert.IsTrue(address.Id > 0);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    [DataRow(4)]
    [DataRow(5)]
    public async Task DepartmentAddressRegisterTest(int reference)
    {   
        var address = new Address($"street{reference}", $"city{reference}", $"state{reference}", $"complement{reference}");
        address.CreationDate = _creationDate;
        await _addressRepository.AddAsync(address);     
        
        var department = new Department(address.Id, $"department{reference}");
        department.CreationDate = _creationDate;
        await _departmentRepository.AddAsync(department);

        Assert.IsTrue(department.Id > 0);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    [DataRow(4)]
    [DataRow(5)]
    public async Task CourseDepartmentAddressRegisterTest(int reference)
    {
        var address = new Address($"street{reference}", $"city{reference}", $"state{reference}", $"complement{reference}");
        address.CreationDate = _creationDate;
        await _addressRepository.AddAsync(address);     
        
        var department = new Department(address.Id, $"department{reference}");
        department.CreationDate = _creationDate;
        await _departmentRepository.AddAsync(department);
        
        var course = new Course(department.Id, $"course{reference}", $"shift{reference}", random.Next(10, 100), $"type{reference}");
        course.CreationDate = _creationDate;
        await _courseRepository.AddAsync(course);

        Assert.IsTrue(course.Id > 0);
    }
    
    [TestMethod]
    [DataRow("Discipline1", 44)]
    [DataRow("Discipline2", 66)]
    [DataRow("Discipline3", 88)]
    [DataRow("Discipline4", 44)]
    [DataRow("Discipline5", 88)]
    public async Task DisciplineRegisterTest(string name, int hours)
    {
        var discipline = new Discipline(name, hours);
        discipline.CreationDate = _creationDate;
        await _disciplineRepository.AddAsync(discipline);

        Assert.IsTrue(discipline.Id > 0);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    [DataRow(4)]
    [DataRow(5)]
    public async Task CourseDisciplineDepartmentAddressRegisterTest(int reference)
    {        
        var address = new Address($"street{reference}", $"city{reference}", $"state{reference}", $"complement{reference}");
        address.CreationDate = _creationDate;
        await _addressRepository.AddAsync(address);     
        
        var department = new Department(address.Id, $"department{reference}");
        department.CreationDate = _creationDate;
        await _departmentRepository.AddAsync(department);
        
        var course = new Course(department.Id, $"course{reference}", $"shift{reference}", random.Next(10, 100), $"type{reference}");
        course.CreationDate = _creationDate;
        await _courseRepository.AddAsync(course);
        
        var discipline = new Discipline($"discipline{reference}", random.Next(20, 80));
        discipline.CreationDate = _creationDate;
        await _disciplineRepository.AddAsync(discipline);
        
        var courseDiscipline = new CourseDiscipline(course.Id, discipline.Id);
        courseDiscipline.CreationDate = _creationDate;
        await _courseDisciplineRepository.AddAsync(courseDiscipline);

        Assert.IsTrue(courseDiscipline.Id > 0);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    [DataRow(4)]
    [DataRow(5)]
    public async Task StudentCourseDepartmentAddressRegisterTest(int reference)
    {
        var address = new Address($"street{reference}", $"city{reference}", $"state{reference}", $"complement{reference}");
        address.CreationDate = _creationDate;
        await _addressRepository.AddAsync(address);     
        
        var department = new Department(address.Id, $"department{reference}");
        department.CreationDate = _creationDate;
        await _departmentRepository.AddAsync(department);
        
        var course = new Course(department.Id, $"course{reference}", $"shift{reference}", random.Next(10, 100), $"type{reference}");
        course.CreationDate = _creationDate;
        await _courseRepository.AddAsync(course);

        var student = new Student(address.Id, course.Id, random.Next(100000, 999999).ToString(), $"name{reference}", $"0{reference}/0{reference}/{1990+reference}");
        student.CreationDate = _creationDate;
        await _studentRepository.AddAsync(student);

        Assert.IsTrue(student.Id > 0);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    [DataRow(4)]
    [DataRow(5)]
    public async Task ProfessorDepartmentAddressRegisterTest(int reference)
    {
        var address = new Address($"street{reference}", $"city{reference}", $"state{reference}", $"complement{reference}");
        address.CreationDate = _creationDate;
        await _addressRepository.AddAsync(address);     
        
        var department = new Department(address.Id, $"department{reference}");
        department.CreationDate = _creationDate;
        await _departmentRepository.AddAsync(department);

        var professor = new Professor(address.Id, department.Id, random.NextInt64(10000000000, 99999999999).ToString(), $"name{reference}", random.Next(1000, 5000), $"0{reference}/0{reference}/{1970+reference}", $"0{reference}/0{reference}/{2000+reference}");
        professor.CreationDate = _creationDate;
        await _professorRepository.AddAsync(professor);

        Assert.IsTrue(professor.Id > 0);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    [DataRow(4)]
    [DataRow(5)]
    public async Task DependentProfessorDepartmentAddressRegisterTest(int reference)
    {
        var address = new Address($"street{reference}", $"city{reference}", $"state{reference}", $"complement{reference}");
        address.CreationDate = _creationDate;
        await _addressRepository.AddAsync(address);     
        
        var department = new Department(address.Id, $"department{reference}");
        department.CreationDate = _creationDate;
        await _departmentRepository.AddAsync(department);

        var professor = new Professor(address.Id, department.Id, random.NextInt64(10000000000, 99999999999).ToString(), $"name{reference}", random.Next(1000, 5000), $"0{reference}/0{reference}/{1970+reference}", $"0{reference}/0{reference}/{2000+reference}");
        professor.CreationDate = _creationDate;
        await _professorRepository.AddAsync(professor);
        
        var dependent = new Dependent(professor.Id, $"name{reference}", $"relationship{reference}", $"0{reference}/0{reference}/{1970+reference}");
        dependent.CreationDate = _creationDate;
        await _dependentRepository.AddAsync(dependent);

        Assert.IsTrue(dependent.Id > 0);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    [DataRow(4)]
    [DataRow(5)]
    public async Task TitleProfessorDepartmentAddressRegisterTest(int reference)
    {
        var address = new Address($"street{reference}", $"city{reference}", $"state{reference}", $"complement{reference}");
        address.CreationDate = _creationDate;
        await _addressRepository.AddAsync(address);     
        
        var department = new Department(address.Id, $"department{reference}");
        department.CreationDate = _creationDate;
        await _departmentRepository.AddAsync(department);

        var professor = new Professor(address.Id, department.Id, random.NextInt64(10000000000, 99999999999).ToString(), $"name{reference}", random.Next(1000, 5000), $"0{reference}/0{reference}/{1970+reference}", $"0{reference}/0{reference}/{2000+reference}");
        professor.CreationDate = _creationDate;
        await _professorRepository.AddAsync(professor);

        var title = new Title(professor.Id, reference, $"course{reference}", $"institution{reference}");
        title.CreationDate = _creationDate;
        await _titleRepository.AddAsync(title);

        Assert.IsTrue(title.Id > 0);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    [DataRow(4)]
    [DataRow(5)]
    public async Task OfferStudentCourseProfessorDepartmentAddressDisciplineRegisterTest(int reference)
    {
        var discipline = new Discipline($"discipline{reference}", random.Next(20, 80));
        discipline.CreationDate = _creationDate;
        await _disciplineRepository.AddAsync(discipline);
        
        var address = new Address($"street{reference}", $"city{reference}", $"state{reference}", $"complement{reference}");
        address.CreationDate = _creationDate;
        await _addressRepository.AddAsync(address);     
        
        var department = new Department(address.Id, $"department{reference}");
        department.CreationDate = _creationDate;
        await _departmentRepository.AddAsync(department);

        var professor = new Professor(address.Id, department.Id, random.NextInt64(10000000000, 99999999999).ToString(), $"name{reference}", random.Next(1000, 5000), $"0{reference}/0{reference}/{1970+reference}", $"0{reference}/0{reference}/{2000+reference}");
        professor.CreationDate = _creationDate;
        await _professorRepository.AddAsync(professor);

        var course = new Course(department.Id, $"course{reference}", $"shift{reference}", random.Next(10, 100), $"type{reference}");
        course.CreationDate = _creationDate;
        await _courseRepository.AddAsync(course);

        var student = new Student(address.Id, course.Id, random.Next(100000, 999999).ToString(), $"name{reference}", $"0{reference}/0{reference}/{1990+reference}");
        student.CreationDate = _creationDate;
        await _studentRepository.AddAsync(student);

        var offer = new Offer(student.Id, professor.Id, discipline.Id, reference+2015, random.Next(1, 2), reference+5, $"frequency{reference}");
        offer.CreationDate = _creationDate;
        await _offerRepository.AddAsync(offer);

        Assert.IsTrue(offer.Id > 0);
    }
}