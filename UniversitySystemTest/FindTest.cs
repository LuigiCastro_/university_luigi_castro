namespace UniversitySystemTest;

[TestClass]
public class FindTest
{
    public readonly IAddressRepository _addressRepository;
    public readonly ICourseRepository _courseRepository;
    public readonly ICourseDisciplineRepository _courseDisciplineRepository;
    public readonly IDepartmentRepository _departmentRepository;
    public readonly IDependentRepository _dependentRepository;
    public readonly IDisciplineRepository _disciplineRepository;
    public readonly IOfferRepository _offerRepository;
    public readonly IProfessorRepository _professorRepository;
    public readonly IStudentRepository _studentRepository;
    public readonly ITitleRepository _titleRepository;    

    public FindTest()
    {
        _addressRepository = DependencyInjection.Provider().GetService<IAddressRepository>();
        _courseRepository = DependencyInjection.Provider().GetService<ICourseRepository>();
        _courseDisciplineRepository = DependencyInjection.Provider().GetService<ICourseDisciplineRepository>();
        _departmentRepository = DependencyInjection.Provider().GetService<IDepartmentRepository>();
        _dependentRepository = DependencyInjection.Provider().GetService<IDependentRepository>();
        _disciplineRepository = DependencyInjection.Provider().GetService<IDisciplineRepository>();
        _offerRepository = DependencyInjection.Provider().GetService<IOfferRepository>();
        _professorRepository = DependencyInjection.Provider().GetService<IProfessorRepository>();
        _studentRepository = DependencyInjection.Provider().GetService<IStudentRepository>();
        _titleRepository = DependencyInjection.Provider().GetService<ITitleRepository>();
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task AddressFindById(int id)
    {
        var address = await _addressRepository.FindAsync(id);
        Assert.IsNotNull(address);
        Assert.AreEqual(id, address.Id);
    }
    
    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task AddressFindAll(int id)
    {
        var address = await _addressRepository.ListAsync();
        Assert.IsTrue(address.Count > 0);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task CourseFindById(int id)
    {
        var course = await _courseRepository.FindAsync(id);
        Assert.IsNotNull(course);
        Assert.AreEqual(id, course.Id);
    }
    
    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task CourseFindAll(int id)
    {
        var course = await _courseRepository.ListAsync();
        Assert.IsTrue(course.Count > 0);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task CourseDisciplineFindById(int id)
    {
        var courseDiscipline = await _courseDisciplineRepository.FindAsync(id);
        Assert.IsNotNull(courseDiscipline);
        Assert.AreEqual(id, courseDiscipline.Id);
    }
    
    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task CourseDisciplineFindAll(int id)
    {
        var courseDiscipline = await _courseDisciplineRepository.ListAsync();
        Assert.IsTrue(courseDiscipline.Count > 0);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task DepartmentFindById(int id)
    {
        var department = await _departmentRepository.FindAsync(id);
        Assert.IsNotNull(department);
        Assert.AreEqual(id, department.Id);
    }
    
    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task DepartmentFindAll(int id)
    {
        var department = await _departmentRepository.ListAsync();
        Assert.IsTrue(department.Count > 0);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task DependentFindById(int id)
    {
        var dependent = await _dependentRepository.FindAsync(id);
        Assert.IsNotNull(dependent);
        Assert.AreEqual(id, dependent.Id);
    }
    
    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task DependentFindAll(int id)
    {
        var dependent = await _dependentRepository.ListAsync();
        Assert.IsTrue(dependent.Count > 0);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task DisciplineFindById(int id)
    {
        var discipline = await _disciplineRepository.FindAsync(id);
        Assert.IsNotNull(discipline);
        Assert.AreEqual(id, discipline.Id);
    }
    
    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task DisciplineFindAll(int id)
    {
        var discipline = await _disciplineRepository.ListAsync();
        Assert.IsTrue(discipline.Count > 0);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task OfferFindById(int id)
    {
        var offer = await _offerRepository.FindAsync(id);
        Assert.IsNotNull(offer);
        Assert.AreEqual(id, offer.Id);
    }
    
    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task OfferFindAll(int id)
    {
        var offer = await _offerRepository.ListAsync();
        Assert.IsTrue(offer.Count > 0);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task ProfessorFindById(int id)
    {
        var professor = await _professorRepository.FindAsync(id);
        Assert.IsNotNull(professor);
        Assert.AreEqual(id, professor.Id);
    }
    
    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task ProfessorFindAll(int id)
    {
        var professor = await _professorRepository.ListAsync();
        Assert.IsTrue(professor.Count > 0);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task StudentFindById(int id)
    {
        var student = await _studentRepository.FindAsync(id);
        Assert.IsNotNull(student);
        Assert.AreEqual(id, student.Id);
    }
    
    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task StudentFindAll(int id)
    {
        var student = await _studentRepository.ListAsync();
        Assert.IsTrue(student.Count > 0);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task TitleFindById(int id)
    {
        var title = await _titleRepository.FindAsync(id);
        Assert.IsNotNull(title);
        Assert.AreEqual(id, title.Id);
    }
    
    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    [DataRow(3)]
    public async Task TitleFindAll(int id)
    {
        var title = await _titleRepository.ListAsync();
        Assert.IsTrue(title.Count > 0);
    }

}