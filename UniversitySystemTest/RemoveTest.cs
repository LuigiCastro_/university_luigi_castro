namespace UniversitySystemTest;

[TestClass]
public class RemoveTest
{
    public readonly IAddressRepository _addressRepository;
    public readonly ICourseRepository _courseRepository;
    public readonly ICourseDisciplineRepository _courseDisciplineRepository;
    public readonly IDepartmentRepository _departmentRepository;
    public readonly IDependentRepository _dependentRepository;
    public readonly IDisciplineRepository _disciplineRepository;
    public readonly IOfferRepository _offerRepository;
    public readonly IProfessorRepository _professorRepository;
    public readonly IStudentRepository _studentRepository;
    public readonly ITitleRepository _titleRepository;    

    public readonly DateTime _alterationDate = DateTime.Now;
    public readonly Random random = new();

    public RemoveTest()
    {
        _addressRepository = DependencyInjection.Provider().GetService<IAddressRepository>();
        _courseRepository = DependencyInjection.Provider().GetService<ICourseRepository>();
        _courseDisciplineRepository = DependencyInjection.Provider().GetService<ICourseDisciplineRepository>();
        _departmentRepository = DependencyInjection.Provider().GetService<IDepartmentRepository>();
        _dependentRepository = DependencyInjection.Provider().GetService<IDependentRepository>();
        _disciplineRepository = DependencyInjection.Provider().GetService<IDisciplineRepository>();
        _offerRepository = DependencyInjection.Provider().GetService<IOfferRepository>();
        _professorRepository = DependencyInjection.Provider().GetService<IProfessorRepository>();
        _studentRepository = DependencyInjection.Provider().GetService<IStudentRepository>();
        _titleRepository = DependencyInjection.Provider().GetService<ITitleRepository>();
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task AddressRemoveTest(int reference)
    {
        var address = await _addressRepository.FindAsync(reference);
        address.Active = false;
        address.AlterationDate = _alterationDate;

        await _addressRepository.EditAsync(address);

        var alteredAddress = await _addressRepository.FindAsync(reference);

        Assert.IsFalse(alteredAddress.Active);
        Assert.AreEqual(address.Active, alteredAddress.Active);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task CourseRemoveTest(int reference)
    {
        var course = await _courseRepository.FindAsync(reference);
        course.Active = false;
        course.AlterationDate = _alterationDate;

        await _courseRepository.EditAsync(course);

        var alteredCourse = await _courseRepository.FindAsync(reference);

        Assert.IsFalse(alteredCourse.Active);
        Assert.AreEqual(course.Active, alteredCourse.Active);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task CourseDisciplineRemoveTest(int reference)
    {        
        var courseDiscipline = await _courseDisciplineRepository.FindAsync(reference);
        courseDiscipline.Active = false;
        courseDiscipline.AlterationDate = _alterationDate;

        await _courseDisciplineRepository.EditAsync(courseDiscipline);

        var alteredCourseDiscipline = await _courseDisciplineRepository.FindAsync(reference);

        Assert.IsFalse(alteredCourseDiscipline.Active);
        Assert.AreEqual(courseDiscipline.Active, alteredCourseDiscipline.Active);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task DepartmentRemoveTest(int reference)
    {        
        var department = await _departmentRepository.FindAsync(reference);
        department.Active = false;
        department.AlterationDate = _alterationDate;

        await _departmentRepository.EditAsync(department);

        var alteredDepartment = await _departmentRepository.FindAsync(reference);

        Assert.IsFalse(alteredDepartment.Active);
        Assert.AreEqual(department.Active, alteredDepartment.Active);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task DependentRemoveTest(int reference)
    {        
        var dependent = await _dependentRepository.FindAsync(reference);
        dependent.Active = false;
        dependent.AlterationDate = _alterationDate;

        await _dependentRepository.EditAsync(dependent);

        var alteredDependent = await _dependentRepository.FindAsync(reference);

        Assert.IsFalse(dependent.Active);
        Assert.AreEqual(dependent.Active, alteredDependent.Active);
    }
    
    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task DisciplineRemoveTest(int reference)
    {        
        var discipline = await _disciplineRepository.FindAsync(reference);
        discipline.Active = false;
        discipline.AlterationDate = _alterationDate;

        await _disciplineRepository.EditAsync(discipline);

        var alteredDiscipline = await _disciplineRepository.FindAsync(reference);

        Assert.IsFalse(alteredDiscipline.Active);
        Assert.AreEqual(discipline.Active, alteredDiscipline.Active);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task OfferRemoveTest(int reference)
    {        
        var offer = await _offerRepository.FindAsync(reference);
        offer.Active = false;
        offer.AlterationDate = _alterationDate;

        await _offerRepository.EditAsync(offer);

        var alteredOffer = await _offerRepository.FindAsync(reference);

        Assert.IsFalse(alteredOffer.Active);
        Assert.AreEqual(offer.Active, alteredOffer.Active);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task ProfessorRemoveTest(int reference)
    {        
        var professor = await _professorRepository.FindAsync(reference);
        professor.Active = false;
        professor.AlterationDate = _alterationDate;

        await _professorRepository.EditAsync(professor);

        var alteredProfessor = await _professorRepository.FindAsync(reference);

        Assert.IsFalse(alteredProfessor.Active);
        Assert.AreEqual(professor.Active, alteredProfessor.Active);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task StudentRemoveTest(int reference)
    {        
        var student = await _studentRepository.FindAsync(reference);
        student.Active = false;
        student.AlterationDate = _alterationDate;

        await _studentRepository.EditAsync(student);

        var alteredStudent = await _studentRepository.FindAsync(reference);

        Assert.IsFalse(alteredStudent.Active);
        Assert.AreEqual(student.Active, alteredStudent.Active);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task TitleRemoveTest(int reference)
    {        
        var title = await _titleRepository.FindAsync(reference);
        title.Active = false;
        title.AlterationDate = _alterationDate;

        await _titleRepository.EditAsync(title);

        var alteredTitle = await _titleRepository.FindAsync(reference);

        Assert.IsFalse(alteredTitle.Active);
        Assert.AreEqual(title.Active, alteredTitle.Active);
    }
    
}