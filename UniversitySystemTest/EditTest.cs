namespace UniversitySystemTest;

[TestClass]
public class EditTest
{
    public readonly IAddressRepository _addressRepository;
    public readonly ICourseRepository _courseRepository;
    public readonly ICourseDisciplineRepository _courseDisciplineRepository;
    public readonly IDepartmentRepository _departmentRepository;
    public readonly IDependentRepository _dependentRepository;
    public readonly IDisciplineRepository _disciplineRepository;
    public readonly IOfferRepository _offerRepository;
    public readonly IProfessorRepository _professorRepository;
    public readonly IStudentRepository _studentRepository;
    public readonly ITitleRepository _titleRepository;    

    public readonly DateTime _alterationDate = DateTime.Now;
    public readonly Random random = new();

    public EditTest()
    {
        _addressRepository = DependencyInjection.Provider().GetService<IAddressRepository>();
        _courseRepository = DependencyInjection.Provider().GetService<ICourseRepository>();
        _courseDisciplineRepository = DependencyInjection.Provider().GetService<ICourseDisciplineRepository>();
        _departmentRepository = DependencyInjection.Provider().GetService<IDepartmentRepository>();
        _dependentRepository = DependencyInjection.Provider().GetService<IDependentRepository>();
        _disciplineRepository = DependencyInjection.Provider().GetService<IDisciplineRepository>();
        _offerRepository = DependencyInjection.Provider().GetService<IOfferRepository>();
        _professorRepository = DependencyInjection.Provider().GetService<IProfessorRepository>();
        _studentRepository = DependencyInjection.Provider().GetService<IStudentRepository>();
        _titleRepository = DependencyInjection.Provider().GetService<ITitleRepository>();
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task AddressEditTest(int reference)
    {
        var address = await _addressRepository.FindAsync(reference);
        address.Street = $"street{random.Next(1, 100)}";
        address.AlterationDate = _alterationDate;

        await _addressRepository.EditAsync(address);

        var alteredAddress = await _addressRepository.FindAsync(reference);

        Assert.AreEqual(address.Street, alteredAddress.Street);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task CourseEditTest(int reference)
    {
        var course = await _courseRepository.FindAsync(reference);
        course.Name = $"name{random.Next(1, 100)}";
        course.AlterationDate = _alterationDate;

        await _courseRepository.EditAsync(course);

        var alteredCourse = await _courseRepository.FindAsync(reference);

        Assert.AreEqual(course.Name, alteredCourse.Name);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task CourseDisciplineEditTest(int reference)
    {        
        var courseDiscipline = await _courseDisciplineRepository.FindAsync(reference);
        courseDiscipline.CourseId = random.Next(1, 5);
        courseDiscipline.AlterationDate = _alterationDate;

        await _courseDisciplineRepository.EditAsync(courseDiscipline);

        var alteredCourseDiscipline = await _courseDisciplineRepository.FindAsync(reference);

        Assert.AreEqual(courseDiscipline.CourseId, alteredCourseDiscipline.CourseId);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task DepartmentEditTest(int reference)
    {        
        var department = await _departmentRepository.FindAsync(reference);
        department.Name = $"name{random.Next(1, 100)}";
        department.AlterationDate = _alterationDate;

        await _departmentRepository.EditAsync(department);

        var alteredDepartment = await _departmentRepository.FindAsync(reference);

        Assert.AreEqual(department.Name, alteredDepartment.Name);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task DependentEditTest(int reference)
    {        
        var dependent = await _dependentRepository.FindAsync(reference);
        dependent.Name = $"name{random.Next(1, 100)}";
        dependent.AlterationDate = _alterationDate;

        await _dependentRepository.EditAsync(dependent);

        var alteredDependent = await _dependentRepository.FindAsync(reference);

        Assert.AreEqual(dependent.Name, alteredDependent.Name);
    }
    
    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task DisciplineEditTest(int reference)
    {        
        var discipline = await _disciplineRepository.FindAsync(reference);
        discipline.Name = $"name{random.Next(1, 100)}";
        discipline.AlterationDate = _alterationDate;

        await _disciplineRepository.EditAsync(discipline);

        var alteredDiscipline = await _disciplineRepository.FindAsync(reference);

        Assert.AreEqual(discipline.Name, alteredDiscipline.Name);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task OfferEditTest(int reference)
    {        
        var offer = await _offerRepository.FindAsync(reference);
        offer.DisciplineId = random.Next(1, 5);
        offer.AlterationDate = _alterationDate;

        await _offerRepository.EditAsync(offer);

        var alteredOffer = await _offerRepository.FindAsync(reference);

        Assert.AreEqual(offer.DisciplineId, alteredOffer.DisciplineId);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task ProfessorEditTest(int reference)
    {        
        var professor = await _professorRepository.FindAsync(reference);
        professor.CPF = random.NextInt64(10000000000, 99999999999).ToString();
        professor.AlterationDate = _alterationDate;

        await _professorRepository.EditAsync(professor);

        var alteredProfessor = await _professorRepository.FindAsync(reference);

        Assert.AreEqual(professor.CPF, alteredProfessor.CPF);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task StudentEditTest(int reference)
    {        
        var student = await _studentRepository.FindAsync(reference);
        student.Name = $"{random.Next(1, 100)}";
        student.AlterationDate = _alterationDate;

        await _studentRepository.EditAsync(student);

        var alteredStudent = await _studentRepository.FindAsync(reference);

        Assert.AreEqual(student.Name, alteredStudent.Name);
    }

    [TestMethod]
    [DataRow(1)]
    [DataRow(2)]
    public async Task TitleEditTest(int reference)
    {        
        var title = await _titleRepository.FindAsync(reference);
        title.Degree = random.Next(1, 100);
        title.AlterationDate = _alterationDate;

        await _titleRepository.EditAsync(title);

        var alteredTitle = await _titleRepository.FindAsync(reference);

        Assert.AreEqual(title.Degree, alteredTitle.Degree);
    }
}