global using Microsoft.VisualStudio.TestTools.UnitTesting;
global using UniversitySystem.Extensions;
global using UniversitySystem.Interfaces;
global using UniversitySystem.Entities;
global using UniversitySystem.Repositories;
global using UniversitySystem.Contexts;
global using Microsoft.Extensions.DependencyInjection;
